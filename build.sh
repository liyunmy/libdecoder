#!/bin/bash

SRCDIR="src/"
BINDIR="bin/"
TESTDIR="../test/"

cd $SRCDIR &&
make -f makefile 

cd $TESTDIR &&
make -f makefile 
