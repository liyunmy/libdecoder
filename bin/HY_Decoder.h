#ifndef _HY_DECODER_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _HY_DECODER_H_

#define  HY_DECODER_API 

#define __stdcall 

//*************接口************
HY_DECODER_API int HY_Decoder_SDKVer(char *ver);
HY_DECODER_API int HY_Decoder_Init();
HY_DECODER_API int HY_DecoderCreateModule();
//useHardDec是否使用硬件解码，0表示使用软件解码，非0表示使用硬件解码
HY_DECODER_API int HY_DecoderCreateModuleEx(int useHardDec); 
HY_DECODER_API int HY_DecoderInit(int id);
HY_DECODER_API int HY_DecoderPushData(int id,BYTE *buf,int len);
HY_DECODER_API int HY_DecoderOpen(int id);
HY_DECODER_API int HY_ReleaseDecoderModule(int id);
HY_DECODER_API int HY_RegDecoderCallbackFun(int id,void (__stdcall *fDecodeVDataFun)(int,int,BYTE *,int,int));
HY_DECODER_API int HY_RegDecoderCallbackFunEx(int id,void (__stdcall *fDecodeVDataFunEx)(int,int,BYTE *,int,BYTE *,int,BYTE *,int,int,int));



#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _HY_DECODER_H_ */

