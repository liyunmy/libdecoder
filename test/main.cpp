#include "stdafx.h"
#include "HY_LiveClient.h"
#include "HY_Decoder.h"
#include "senddata.h"
#define SOK 0

int s_id1=0;
int d_id1=0;

int s_id2=0;
int d_id2=0;

FILE *pf=NULL;

int LiveClientVStreamCallbackFun(int id ,BYTE* pBuf, int len, Uint32 ts, WORD seq, void *pUserData)
{
	//printf("id = %d len:%d\n",id,len);
	//fwrite(pBuf,1,len,pf);
	if(id == s_id1)HY_DecoderPushData(d_id1,pBuf,len);
	if(id == s_id2)HY_DecoderPushData(d_id2,pBuf,len);
	
	return SOK;
}


int LiveClientAStreamCallbackFun(int id ,BYTE* pBuf, int len, void *pUserData)
{
	//OS_printf("A id = %d len:%d\n",id,len);
	return 0;
}

int LiveClientEventFun(int id ,int Envent, void *userpara)
{
	printf("id:%d Envent:%d\n",id,Envent);
	return SOK;
}


void DecodeVDataFun(int id,int type,BYTE *buf,int width,int height)
{
	printf("id = %d width:%d height:%d\n",id,width,height);
	if(id == d_id1)senddata(buf,352*288*3/2,FLAG_VIDEO_420);
}


int main()
{	

	printf("start 0815\n");
	//pf=fopen("1.H264","wb");
	
	initsock("192.168.1.14",PORT_SOCKET_CLIENT,PORT_SOCKET_SERVER);
	
	HY_LiveClient_Init();
	s_id1 = HY_LiveClientCreateStream();
	s_id2 = HY_LiveClientCreateStream();

	HY_Decoder_Init();
	d_id1 = HY_DecoderCreateModule();
	d_id2 = HY_DecoderCreateModule();

	//***********第一路***********
	RTSPParam mRTSPParam;
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","10.8.1.164");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","hk123456");
	HY_RegLiveClientAStreamCallbackFun(s_id1,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id1,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id1,LiveClientVStreamCallbackFun);
	//HY_LiveClientOpenStream(s_id1,(char*)"rtsp://admin:admin@192.168.13.221/minorvideo_and_audio",mRTSPParam);
	HY_LiveClientOpenStream(s_id1,(char*)"rtsp://10.8.1.164:554/Streaming/Channels/1?transportmode=unicast&profile=Profile_1",mRTSPParam);

	//***********第二路***********
	//RTSPParam mStreamParam;
	
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","10.8.1.164");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","hk123456");
	HY_RegLiveClientAStreamCallbackFun(s_id2,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id2,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id2,LiveClientVStreamCallbackFun);
	HY_LiveClientOpenStream(s_id2,(char*)"rtsp://10.8.1.164:554/Streaming/Channels/1?transportmode=unicast&profile=Profile_1",mRTSPParam);

	//usleep(3*1000*1000);

	//***********第一路***********
	
	HY_DecoderInit(d_id1);
	HY_RegDecoderCallbackFun(d_id1,DecodeVDataFun);
	HY_DecoderOpen(d_id1);
	
	//***********第二路***********
	
	HY_DecoderInit(d_id2);
	HY_RegDecoderCallbackFun(d_id2,DecodeVDataFun);
	HY_DecoderOpen(d_id2);
	
	while(1)
	{
		usleep(10*1000*1000);
	}
	return 0;
}