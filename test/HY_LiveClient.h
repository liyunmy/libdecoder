#ifndef _HY_LIVESERVER_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _HY_LIVESERVER_H_

#define  HY_LIVE_API 

#define __stdcall 

#ifndef BYTE
typedef unsigned char BYTE;            ///< Unsigned  char
#endif

#ifndef Uint8
typedef unsigned char Uint8;            ///< Unsigned  8-bit integer
#endif

#ifndef Uint16
typedef unsigned short Uint16;          ///< Unsigned 16-bit integer
#endif

#ifndef Uint32
typedef unsigned int Uint32;            ///< Unsigned 32-bit integer
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

#define RTSP_EVE_STOPPED    0
#define RTSP_EVE_CONNECTING 1
#define RTSP_EVE_CONNFAIL   2
#define RTSP_EVE_CONNSUCC   3
#define RTSP_EVE_NOSIGNAL   4
#define RTSP_EVE_RESUME     5
#define RTSP_EVE_AUTHFAILED 6


typedef struct{

	int useTCP;
	//是否使用TCP传输码流, 0 UDP 1 TCP
	char IP[16];
	char UserName[32];
	char Password[32];

}RTSPParam;


HY_LIVE_API int HY_LiveClient_SDKVer(char *ver);

HY_LIVE_API int HY_LiveClient_Init();

HY_LIVE_API int HY_LiveClientCreateStream();

HY_LIVE_API int HY_LiveClientOpenStream(int id,char *URI,RTSPParam mRTSPParam);

HY_LIVE_API int HY_LiveClientSetParamStream(int id,RTSPParam m_RTSPParam,char *url);

HY_LIVE_API int HY_LiveClientResetStream(int id,int type);//type=0视频流,1音频流

HY_LIVE_API int HY_LiveClientPlayStream(int id);

HY_LIVE_API int HY_LiveClientStopStream(int id);

HY_LIVE_API int HY_LiveClientCloseStream(int id);

HY_LIVE_API int HY_LiveClientReleaseStream(int id);

HY_LIVE_API int  HY_RegLiveClientEventCallbackFun(int id,void *pUserData,int (__stdcall*fLiveClientEventFun)(int ,int, void *));

HY_LIVE_API int HY_RegLiveClientVStreamCallbackFun(int id,int (__stdcall*fLiveClientVStreamFun)(int id ,BYTE* pBuf, int len, Uint32 ts, WORD seq, void *pUserData));

HY_LIVE_API int HY_RegLiveClientAStreamCallbackFun(int id,int (__stdcall*fLiveClientAStreamFun)(int id ,BYTE* pBuf, int len, void *pUserData));

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _HY_LIVESERVER_H_ */

