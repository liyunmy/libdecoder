
// senddata.h: 
//
//////////////////////////////////////////////////////////////////////

#ifndef __SENDDATA_H__
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define __SENDDATA_H__	


#define BYTE unsigned char


#define PORT_SOCKET_CLIENT 6801
#define PORT_SOCKET_SERVER 6800

#define SIZE_VIDEO_PACKED 1500
#define SIZE_VIDEO_BUFFER (SIZE_VIDEO_PACKED*300)  //400KB
#define SIZE_BUFFER_NUM  2

//pack flag
#define FLAG_AUDIO		0x3131
#define FLAG_VIDEO_422		0x3132
#define FLAG_CMD		0x3133
#define FLAG_AUDIOACK	0x3134
#define FLAG_VIDEOACK	0x3135
#define FLAG_VIDEO_420		0x3136
#define FLAG_VIDEO_JPEG		0x3137

typedef struct
{
	short flag; 
	BYTE id;	
	BYTE subid;  
	unsigned int data_size_total;	
	unsigned short data_size;	
	unsigned short align;
	BYTE data[SIZE_VIDEO_PACKED]; 
}AVIO_VIDEO;

void initsock(char *pRemIP,unsigned short uRemPort,unsigned short uLocPort);
void senddata(BYTE* pPicBuf,int ilen,int flag);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __SENDDATA_H__ */

