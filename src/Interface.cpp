#include "stdafx.h"

#define __STDC_CONSTANT_MACROS

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
};
#include "Decoder.h"

#include "senddata.h"
#define DECODER_EXPORTS
#ifdef DECODER_EXPORTS
#define HY_DECODER_API 
#else
#define  HY_DECODER_API 
#endif
#include "Interface.h"

#define SOK      0  ///< Status : OK
#define EFAIL   -1  ///< Status : Generic error

#define MAX_CHANNEL 32

CDecoder *pCDecoder[MAX_CHANNEL]={
	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
};

OS_TskHndl DecodeHandl;
void *DecodeTask(void *param);

extern int g_DecoderNum;

//**************相关函数******************
static int OS_tskCreate(OS_TskHndl *pPrc, OS_ThrEntryFunc func,Uint32 initState,void *appData,Uint32 pri=0,Uint32 stackSize=0);

/*
LC_Init();
//LC_Deinit();
OpenStreamParam_t m_OpenStreamParam_t;
m_OpenStreamParam_t.useTCP = 1;  //TCP 
m_OpenStreamParam_t.streamType = 0 ;//标准
m_OpenStreamParam_t.username = "admin";
m_OpenStreamParam_t.password = "admin";

int L_handle = LC_CreateStream(uri,&m_OpenStreamParam_t);
printf("L_handle = %d \n",L_handle);
LC_SetStreamFrameCallback(L_handle,StreamFrameCallbackFun,NULL);
*/
/*
	函数	：	HY_OnvifClient_SDKVer()
	参数	：	ver	-- 输出	--	的版本号
	返回值	：	0
	功能	：	获取版本
*/
HY_DECODER_API int HY_Decoder_SDKVer(char *ver)
{
	//*ver = "V1.01.170228";
	sprintf(ver,"%s","V1.04.170927");
	return SOK ;
}

/*
	函数	：	HY_OnvifClient_Init()
	参数	：	无
	返回值	：	0	--	成功
				-1	--	失败 
	功能	：	初始化并分配空间
*/
HY_DECODER_API int HY_Decoder_Init()
{
	av_register_all();  
	avformat_network_init();
	
	sprintf(DecodeHandl.TskName,"DecodeTask");
	if( OS_tskCreate(&DecodeHandl,DecodeTask,RESUME,NULL) )
	{
		printf("创建DecodeTask失败!\n");			  
	}

	return 1;
}

HY_DECODER_API int HY_DecoderCreateModuleEx(int useHardDec)
{
	int id=0;

	for (id=0;id<MAX_CHANNEL;id++)
	{
		if(pCDecoder[id] == NULL)
		{
			pCDecoder[id] = new CDecoder(useHardDec);
			pCDecoder[id]->id=id;
			printf("初始化Decoder模块:id=%d\n",id);
			return id;
		}
	}
	return EFAIL;
}

HY_DECODER_API int HY_DecoderCreateModule()
{
	return HY_DecoderCreateModuleEx(0);
}

HY_DECODER_API int HY_DecoderInit(int id)
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCDecoder[id] == NULL) return EFAIL;
	pCDecoder[id]->Init();
	return SOK;
}


HY_DECODER_API int HY_DecoderPushData(int id,BYTE *buf,int len)
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	//printf("HY_DecoderPushData id:%d \n",id);
	if (pCDecoder[id] == NULL) return EFAIL;
	pCDecoder[id]->PushVData(buf,len);
	return SOK;
}
HY_DECODER_API int HY_DecoderOpen(int id)
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCDecoder[id] == NULL) return EFAIL;

	//pCDecoder[id]->Open();
	
	return SOK;
}

void *DecodeTask(void *param)
{
	int hasFrameDec;
	while(1)
	{
		if(!g_DecoderNum)
		{
			usleep(500*1000);
		}
		hasFrameDec = 0;
		for(int id=0;id<MAX_CHANNEL;id++)
		{
			if(pCDecoder[id])
			{
				if(0!=pCDecoder[id]->Decode_do()) {
					hasFrameDec = 1;
				}	
			}	
		}
		if(0==hasFrameDec) {//无帧要处理时再休眠，加快解码速度，减少因解码慢导致丢帧
			usleep(10*1000);
		}
	}
	return NULL;
}

HY_DECODER_API int  HY_RegDecoderCallbackFun(int id,void (__stdcall *fDecodeVDataFun)(int,int,BYTE *,int,int))
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCDecoder[id] == NULL) return EFAIL;
	
	pCDecoder[id]->DecodeVDataFun=fDecodeVDataFun;
	return OS_SOK;
}

HY_DECODER_API int HY_RegDecoderCallbackFunEx(int id,void (__stdcall *fDecodeVDataFunEx)(int,int,BYTE *,int,BYTE *,int,BYTE *,int,int,int))
{
	if (id < 0 || id >= MAX_CHANNEL)
	{
		return EFAIL;
	}
	if (pCDecoder[id] == NULL) return EFAIL;
	
	pCDecoder[id]->DecodeVDataFunEx=fDecodeVDataFunEx;
	return OS_SOK;
}


//**************相关函数******************
//任务创建
static int OS_tskCreate(OS_TskHndl *pPrc, OS_ThrEntryFunc func,Uint32 initState,void *appData,Uint32 pri,Uint32 stackSize)
{
  int status=OS_SOK;
  pthread_attr_t thread_attr;
  struct sched_param schedprm;

  status = pthread_attr_init(&thread_attr);
  if(status!=OS_SOK) {
    printf("OSA_thrCreate() - Could not initialize thread attributes\n");
    return status;
  }
  pPrc->TskCount=0;
  
  pPrc->curState      = initState;
  pPrc->pri=pri;
  pPrc->stackSize=stackSize;

    if(stackSize!=OS_THR_STACK_SIZE_DEFAULT)
    pthread_attr_setstacksize(&thread_attr, stackSize);

  status |=pthread_attr_setdetachstate(&thread_attr,PTHREAD_CREATE_DETACHED);
  status |= pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
  status |= pthread_attr_setschedpolicy(&thread_attr, SCHED_FIFO);
    
  if((int)pri>OS_THR_PRI_MAX)   
    pri=(int)OS_THR_PRI_MAX;

  if((int)pri<OS_THR_PRI_MIN)   
    pri=(int)OS_THR_PRI_MIN;
    
  schedprm.sched_priority = pri;
  status |= pthread_attr_setschedparam(&thread_attr, &schedprm);

  if(status!=OS_SOK) {
    printf("OS_thrCreate() - Could not initialize thread attributes\n");
    goto error_exit;
  }

  status = pthread_create(&pPrc->thrHndl, &thread_attr, func, (void*)appData);
  
  if(status!=OS_SOK) {
	  printf("OS_thrCreate() - Could not create thread [%d],name:[%s]\n", status,pPrc->TskName);
	}
  
error_exit:  
	pthread_attr_destroy(&thread_attr);

  return status;
}


#if 0
int s_id1=0;
int d_id1=0;

int s_id2=0;
int d_id2=0;

FILE *pf=NULL;

int LiveClientVStreamCallbackFun(int id ,BYTE* pBuf, int len, Uint32 ts, WORD seq, void *pUserData)
{
	//printf("id = %d len:%d\n",id,len);
	//fwrite(pBuf,1,len,pf);
	if(id == s_id1)HY_DecoderPushData(d_id1,pBuf,len);
	if(id == s_id2)HY_DecoderPushData(d_id2,pBuf,len);
	
	return SOK;
}


int LiveClientAStreamCallbackFun(int id ,BYTE* pBuf, int len, void *pUserData)
{
	//OS_printf("A id = %d len:%d\n",id,len);
	return 0;
}

int LiveClientEventFun(int id ,int Envent, void *userpara)
{
	printf("id:%d Envent:%d\n",id,Envent);
	return SOK;
}


void DecodeVDataFun(int id,int type,BYTE *buf,int width,int height)
{
	printf("id = %d width:%d height:%d\n",id,width,height);
	if(id == d_id1)senddata(buf,352*288*3/2,FLAG_VIDEO_420);
}


int main()
{	

	pf=fopen("1.H264","wb");
	
	initsock("192.168.13.136",PORT_SOCKET_CLIENT,PORT_SOCKET_SERVER);
	
	HY_LiveClient_Init();
	s_id1 = HY_LiveClientCreateStream();
	//s_id2 = HY_LiveClientCreateStream();

	HY_Decoder_Init();
	d_id1 = HY_DecoderCreateModule();
	d_id2 = HY_DecoderCreateModule();

	//***********第一路***********
	RTSPParam mRTSPParam;
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","192.168.13.164");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","admin");
	HY_RegLiveClientAStreamCallbackFun(s_id1,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id1,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id1,LiveClientVStreamCallbackFun);
	//HY_LiveClientOpenStream(s_id1,(char*)"rtsp://admin:admin@192.168.13.221/minorvideo_and_audio",mRTSPParam);
	HY_LiveClientOpenStream(s_id1,(char*)"rtsp://admin:hk123456@192.168.13.164/h264/ch1/sub/av_stream",mRTSPParam);

	//***********第二路***********
	//StreamParam mStreamParam;
	/*
	mRTSPParam.useTCP=1;
	snprintf(mRTSPParam.IP,sizeof(mRTSPParam.IP),"%s","192.168.13.228");
	snprintf(mRTSPParam.UserName,sizeof(mRTSPParam.UserName),"%s","admin");
	snprintf(mRTSPParam.Password,sizeof(mRTSPParam.Password),"%s","admin");
	HY_RegLiveClientAStreamCallbackFun(s_id2,LiveClientAStreamCallbackFun);
	HY_RegLiveClientEventCallbackFun(s_id2,NULL,LiveClientEventFun);
	HY_RegLiveClientVStreamCallbackFun(s_id2,LiveClientVStreamCallbackFun);
	HY_LiveClientOpenStream(s_id2,(char*)"rtsp://192.168.13.228/minorvideo_and_audio",mRTSPParam);
*/
	//usleep(3*1000*1000);

	//***********第一路***********

	HY_DecoderInit(d_id1);
	HY_RegDecoderCallbackFun(d_id1,DecodeVDataFun);
	HY_DecoderOpen(d_id1);
	
	//***********第二路***********
	/*
	HY_DecoderInit(d_id2);
	HY_RegDecoderCallbackFun(d_id2,DecodeVDataFun);
	HY_DecoderOpen(d_id2);
	*/
	while(1)
	{
		usleep(10*1000*1000);
	}
	return 0;
}
#endif
