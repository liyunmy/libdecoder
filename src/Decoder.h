#ifndef _DECODER_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#define _DECODER_H_

#define __stdcall

#ifndef BYTE
typedef unsigned char BYTE;            ///< Unsigned  char
#endif

#ifndef Uint8
typedef unsigned char Uint8;            ///< Unsigned  8-bit integer
#endif

#ifndef Uint16
typedef unsigned short Uint16;          ///< Unsigned 16-bit integer
#endif

#ifndef Uint32
typedef unsigned int Uint32;            ///< Unsigned 32-bit integer
#endif

#ifndef WORD
typedef unsigned short WORD;
#endif

//*******************任务****************
#define OS_THR_PRI_MAX                 sched_get_priority_max(SCHED_FIFO)
#define OS_THR_PRI_MIN                 sched_get_priority_min(SCHED_FIFO)

#define OS_THR_PRI_DEFAULT             ( OSA_THR_PRI_MIN + (OSA_THR_PRI_MAX-OSA_THR_PRI_MIN)/2 )

#define OS_THR_STACK_SIZE_DEFAULT      0

#define OS_SOK      0  ///< Status : OK
#define OS_EFAIL   -1  ///< Status : Generic error

//任务状态
typedef enum _TSKTYPE
{
	SUSPEND=0,
	RESUME =1
}TSKTYPE;

typedef void * (*OS_ThrEntryFunc)(void *);
typedef pthread_attr_t OS_ThrHndle;

typedef struct _OS_TskHndl{

  char TskName[32];
  pthread_t thrHndl;      ///< OS thread handle
  Uint32 curState;          ///< Task state as defined by user
  Uint32 TskCount;			//task counter
  Uint32 pri;
  Uint32 stackSize;
} OS_TskHndl;

//任务锁
typedef struct {
	pthread_mutex_t lock;
} OS_MutexHndl;

//**********************************************************

#define MAXBUFSIZE 16
#define VIDEOTYPE 0x01
#define VDATASIZE 200*1024
#define VDATAMAXSIZE 200*1024

#define VDATA_READ_FLAG	0
#define VDATA_WRITE_FLAG	1

typedef struct _vdata{
	int id;
	int flag;
	unsigned int seqNum;
	int len;
	char *pdata;	
}vdata;


class CDecoder
{
public:
	CDecoder(int useHardDec);
	virtual ~CDecoder(void);

	//OS_MutexHndl Mutex_GetVData;

	int id;
	int is_start;
	int is_decThreadExit;
	int videoindex;
	/*
	AVFormatContext *pFormatCtx;  
	AVCodecContext  *pCodecCtx;  
	AVCodec         *pCodec;  
	unsigned char *aviobuffer;
	AVIOContext *avio;
	
	AVFrame *pFrame;	 
	AVPacket *packet;
	*/

	AVCodec *pCodec;  
    AVCodecContext *pCodecCtx;  
    AVCodecParserContext *pCodecParserCtx;  
   
    AVFrame *pFrame;  
	AVPacket packet;	
    //const int in_buffer_size=4096;  
    //uint8_t in_buffer[in_buffer_size + FF_INPUT_BUFFER_PADDING_SIZE]={0};  
	uint8_t *in_buffer;
    uint8_t *m_cur_ptr;  
	int m_cur_size;  
	int first_time;
    //AVPacket *packet;
	AVCodecID codec_id; 
	//struct SwsContext *img_convert_ctx; 

	OS_TskHndl TaskExitHandl;
	//******缓存数据*****
	vdata *pVDataBuf;
	Uint32 p_Pos ;//数据压入位置
    Uint32 u_Pos ;//数据使用位置
	//int idecErrCount;//解码次数统计
    BYTE  *pVDataYUV420;
	int needWaitIFrame;//出错或丢缓存时需要等待下一个I帧，否则会花屏
	unsigned int pushSeqNum;
	unsigned int decSeqNum;
public:	
	int Open(void);	
	int GetVDataSize();
	int PushVData(uint8_t *buf,int len);	
	int GetVData(uint8_t *buf,int *len,unsigned int*pseqNum);
	int Init();
	int Decode_do();
	void (__stdcall * DecodeVDataFun)(int,int,BYTE *,int,int);
	void (__stdcall * DecodeVDataFunEx)(int,int,BYTE *,int,BYTE *,int,BYTE *,int,int,int);
};

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _DECODER_H_ */

