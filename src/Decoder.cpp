#include "stdafx.h"
#define __STDC_CONSTANT_MACROS

#define ENABLE_TIME_TEST	0

#if ENABLE_TIME_TEST
#include <sys/time.h>
#endif


extern "C"
{
#include <libavcodec/avcodec.h>  
#include <libavformat/avformat.h>  
#include <libswscale/swscale.h>
};
#include "Decoder.h"
#include "Interface.h"
#define MAX_CHANNEL 32

int g_DecoderNum=0;

//static int OS_tskMutexCreate(OS_MutexHndl *hndl);
//static int OS_tskMutexDelete(OS_MutexHndl *hndl);
//static void OS_tskMutexLock(OS_MutexHndl *hndl);
//static void OS_tskMutexUnlock(OS_MutexHndl *hndl);
extern CDecoder *pCDecoder[MAX_CHANNEL];



CDecoder::CDecoder(int useHardDec)
{
	pCodec=NULL;
	pCodecCtx=NULL;
	pCodecParserCtx=NULL;
	pFrame=NULL;
	g_DecoderNum++;
	//OS_tskMutexCreate(&Mutex_GetVData);
	pVDataBuf=(vdata *)malloc(MAXBUFSIZE*sizeof(vdata));
	in_buffer=(uint8_t*)malloc(VDATASIZE);
	memset(in_buffer,0,VDATASIZE);
	for(int i=0;i<MAXBUFSIZE;i++)
	{
		memset(pVDataBuf+i,0,sizeof(vdata));
		(pVDataBuf+i)->flag=VDATA_READ_FLAG;
		(pVDataBuf+i)->pdata=(char *)av_malloc(VDATASIZE);
		memset((pVDataBuf+i)->pdata,0,VDATASIZE);
	}
	needWaitIFrame = 1;
	pushSeqNum = 0;
	decSeqNum = 0;
	p_Pos = 0;
    u_Pos = 0; 
	id = 0;
	first_time=0;
	//idecErrCount=0;//解码次数统计
	is_start=1;
	is_decThreadExit=0;
	pVDataYUV420=(BYTE *)malloc(1920*1080*3/2);
	memset(pVDataYUV420,0,1920*1080*3/2);
	DecodeVDataFun = NULL;
	DecodeVDataFunEx = NULL;
	codec_id=AV_CODEC_ID_H264;
	m_cur_size=0;
	m_cur_ptr=(uint8_t*)malloc(VDATASIZE);
	memset(m_cur_ptr,0,VDATASIZE);
	printf("Decoder Init.\n");

	if(0!=useHardDec) {
		pCodec = avcodec_find_decoder_by_name("h264_mmal");
	}
	if (!pCodec) {
		printf("h264_mmal codec has no found.\n");
		pCodec = avcodec_find_decoder(codec_id);  
		if (!pCodec) {  
			printf("Codec not found--id:%d\n", id);  
			return ;  
		}
	} else {
		printf("h264_mmal codec has found.\n");
	}

	 pCodecCtx = avcodec_alloc_context3(pCodec);  
    if (!pCodecCtx){  
        printf("Could not allocate video codec context--id:%d\n",id);  
        return ;  
    }  
  
    pCodecParserCtx=av_parser_init(codec_id);  
    if (!pCodecParserCtx){  
        printf("Could not allocate video parser context--id:%d\n",id);  
        return ;  
    }  

	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {  
        printf("Could not open codec-id:%d\n",id);  
        return ;  
    }   
  
	pFrame = av_frame_alloc(); 
	
	av_init_packet(&packet); 
}


CDecoder::~CDecoder(void)
{
//	free(pFrameYUV->data[0]); 
 //   free(pFrameYUV->data[1]); 
//	free(pFrameYUV->data[2]); 

	av_parser_close(pCodecParserCtx);  
    av_frame_free(&pFrame);  
    avcodec_close(pCodecCtx);  
    av_free(pCodecCtx);  
	//av_free(aviobuffer);
	//avcodec_close(pCodecCtx);  
	//avformat_close_input(&pFormatCtx);
	for(int i=0;i<MAXBUFSIZE;i++)
	{
		free((pVDataBuf+i)->pdata);
	}
	free(pVDataBuf);
	free(pVDataYUV420);
	printf("id:%d ~CDecoder\n",id);
}

int CDecoder::Init()
{
	/*
    aviobuffer=(unsigned char *)av_malloc(VDATAMAXSIZE); 

	printf("avcodec_open2 Init-id.\n",id);
    pFrame=av_frame_alloc();  
    packet=(AVPacket *)av_malloc(sizeof(AVPacket)); 
	*/
	return 0;
}


int CDecoder::Decode_do()
{
#if ENABLE_TIME_TEST
	static unsigned long count = 0; 
	static unsigned long totalTime = 0; 
	struct timeval tpstart,tpend;
	unsigned long timeuses=0;
	gettimeofday(&tpstart,0);
#endif
	int got_picture=0;
	int ret=0;
	if(GetVDataSize()>0)
	{  
		uint8_t *cur_ptr=NULL;
		int size=0;
		unsigned int curSeqNum;
		//printf("id:%d GetVDataSize:%d\n",id,GetVDataSize());
		int dlen=GetVData(m_cur_ptr,&m_cur_size,&curSeqNum);
		//printf("id:%d dlen:%d\n",id,dlen);
		if(dlen<=0)return 0;
		if(curSeqNum!=decSeqNum) {//序号不连续，中间有丢数据，需要等待I帧
			needWaitIFrame = 1;
		}
		decSeqNum = curSeqNum+1;//更新到下一个VData的序号
		cur_ptr=m_cur_ptr;
		size=m_cur_size;
		//printf("id:%d size:%d\n",id,size);
		while(size>0)
		{
			//printf("id:%d av_parser_parse2!\n",id);
			int len=av_parser_parse2(  
			pCodecParserCtx, pCodecCtx,  
			&packet.data, &packet.size,  
			cur_ptr , size ,  
			AV_NOPTS_VALUE, AV_NOPTS_VALUE, AV_NOPTS_VALUE);  

			cur_ptr+=len;
			size-=len;
			//printf("id=%d size=%d\n",id,packet.size);
			//packet.data=cur_ptr;
			//packet.size=cur_size;
			
			if(packet.size==0)  
			{
				//printf("continue packet.size-%d!\n",id);
				//av_free_packet(&packet);//不需要释放，ffmpeg底层有管理
				continue;  
			}
			
			//Some Info from AVCodecParserContext  
			/*
			if(first_time==0)
			{
				printf("[Packet]Size:%6d\t",packet.size);  
				switch(pCodecParserCtx->pict_type){  
					case AV_PICTURE_TYPE_I: printf("Type:I\t");break;  
					case AV_PICTURE_TYPE_P: printf("Type:P\t");break;  
					case AV_PICTURE_TYPE_B: printf("Type:B\t");break;  
					default: printf("Type:Other\t");break;  
				}  
				first_time=1;
				//exit(0);
			}
			*/
			if(1==needWaitIFrame)
			{//出现丢帧或解码错误时等待下一个I帧再放进去解码，避免花屏
				if(AV_PICTURE_TYPE_I==pCodecParserCtx->pict_type) {
					needWaitIFrame=0;
					printf("%d Decode_do: needWaitIFrame\n",id);
				} else {
					continue; 
				}
			}

			//printf("Number:%4d\n",pCodecParserCtx->output_picture_number);  
			//printf("avcodec_decode_video2-s\n");
			ret = avcodec_decode_video2(pCodecCtx, pFrame, &got_picture, &packet);  
			//printf("avcodec_decode_video2-e\n");
			if (ret < 0) {  
				printf("Decode Error.id=%d\n",id);  
				needWaitIFrame = 1;
				continue;  
			}  
			if (got_picture) {  
				if(0){  
					printf("\nCodec Full Name:%s\n",pCodecCtx->codec->long_name);  
					printf("width:%d\nheight:%d\n\n",pCodecCtx->width,pCodecCtx->height);  
					//first_time=0;  
				}  
				//Y, U, V  
				int fsize=pCodecCtx->width*pCodecCtx->height;  
				int fwidth=pFrame->width;

				if(DecodeVDataFunEx != NULL ) {
					//树莓派硬件解码时的拷贝的速度较慢，因此提供一个扩展接口，不做内存拷贝，直接返回，
					//用户可根据需要把一些算法跟拷贝操作合并在一起，减少拷贝延迟的影响
					DecodeVDataFunEx(id,VIDEOTYPE
									,pFrame->data[0],pFrame->linesize[0]
									,pFrame->data[1],pFrame->linesize[1]
									,pFrame->data[2],pFrame->linesize[2]
									,pCodecCtx->width,pCodecCtx->height);
				}

				if(DecodeVDataFun != NULL )
				{
					for(int i=0;i<pFrame->height;i++){  
						//fwrite(pFrame->data[0]+pFrame->linesize[0]*i,1,pFrame->width,fp_out);  
						memcpy(pVDataYUV420+i*fwidth,pFrame->data[0]+pFrame->linesize[0]*i,fwidth);//Y
					}  
					for(int i=0;i<pFrame->height/2;i++){  
						//fwrite(pFrame->data[1]+pFrame->linesize[1]*i,1,pFrame->width/2,fp_out);  
						memcpy(pVDataYUV420+fsize+i*fwidth/2,pFrame->data[1]+pFrame->linesize[1]*i,fwidth/2);//u
					}  
					for(int i=0;i<pFrame->height/2;i++){  
						//fwrite(pFrame->data[2]+pFrame->linesize[2]*i,1,pFrame->width/2,fp_out);  
						memcpy(pVDataYUV420+fsize+fsize/4+i*fwidth/2,pFrame->data[2]+pFrame->linesize[2]*i,fwidth/2);//v
					}  
					if(DecodeVDataFun != NULL )
					{
						DecodeVDataFun(id,VIDEOTYPE,pVDataYUV420,pCodecCtx->width,pCodecCtx->height);
					}
				}
#if ENABLE_TIME_TEST
				gettimeofday(&tpend,0);
				// 微秒
				timeuses=(tpend.tv_sec-tpstart.tv_sec)*1000000+tpend.tv_usec-tpstart.tv_usec;
				totalTime+=timeuses;
				static unsigned int maxTime = 0;
				static unsigned int minTime = 0xFFFFFFFF;
				if(maxTime<timeuses) maxTime = timeuses;
				if(minTime>timeuses) minTime = timeuses;
				if(++count>=50) {
					printf("Decode_do use time: %uus %uus, min time %uus, max time  %uus\n",totalTime/count,timeuses,minTime,maxTime);
					count = 0;
					totalTime = 0;
					maxTime = 0;
					minTime = 0xFFFFFFFF;
				}
#endif
				//printf("Succeed to decode 1 frame!\n");  
				//av_frame_unref(pFrame);
			}   
			
			//av_free_packet(&packet);//不需要释放，ffmpeg底层有管理
		}
		//av_free_packet(&packet);//不需要释放，ffmpeg底层有管理
		return 1;
	}	
	else {
		return 0;
	}
}

int CDecoder::Open(void)
{
	
	return 0;
}

int CDecoder::PushVData(uint8_t *buf,int len)
{
	unsigned int seqNum;
	//printf("PushVData-s\n");
	if(len<=0)return -1;
	seqNum = pushSeqNum;
	pushSeqNum++;
	if(len>VDATASIZE) {//避免帧数据太大，拷贝崩溃
		printf("%d PushVData Data is Too Big %d.\n",id,len);
		//needWaitIFrame = 1;//是否要等待I帧通过seqNum是否连续来判断，避免pVDataBuf中原来凑巧就有一个I帧
		return -1;
	}
	if(VDATA_READ_FLAG==pVDataBuf[p_Pos].flag) {//判断数据被读走了再写，避免不停写覆盖原来的数据导致花屏
		pVDataBuf[p_Pos].len=len;
		pVDataBuf[p_Pos].seqNum = seqNum;
		memcpy(pVDataBuf[p_Pos].pdata,buf,len);
		pVDataBuf[p_Pos].flag = VDATA_WRITE_FLAG;
		p_Pos++;
		p_Pos%=MAXBUFSIZE;
	} else {
		//解码速度跟不上，缓存满了，丢帧，等待下一个I帧
		printf("%d PushVData Decode Too Slow!\n",id);
		//needWaitIFrame = 1;//是否要等待I帧通过seqNum是否连续来判断，避免pVDataBuf中原来凑巧就有一个I帧
		return -1;
	}
	//printf("PushVData-e\n");
	return len;
}

int CDecoder::GetVDataSize()
{
	//不加锁很难做到一个正确的缓存个数判断，而这里实际使用时只关注有没有缓存，而不关注有多少个缓存，所以简化为
	return (VDATA_WRITE_FLAG==pVDataBuf[u_Pos].flag);
	/*if(p_Pos>=u_Pos)
	{
		return (p_Pos-u_Pos);
	}
	else
	{
		return (p_Pos+MAXBUFSIZE-u_Pos);
	}*/
}

int CDecoder::GetVData(uint8_t *buf,int *len,unsigned int*pseqNum)
{
	if(VDATA_WRITE_FLAG==pVDataBuf[u_Pos].flag) {
		memcpy(buf,pVDataBuf[u_Pos].pdata,pVDataBuf[u_Pos].len);
		*len=pVDataBuf[u_Pos].len;
		if(pseqNum) *pseqNum = pVDataBuf[u_Pos].seqNum;
		pVDataBuf[u_Pos].flag = VDATA_READ_FLAG;
		u_Pos++;
		u_Pos%=MAXBUFSIZE;
		return *len;
	} else {
		*len=0;
		return -1;
	}
}


HY_DECODER_API int  HY_ReleaseDecoderModule(int id)
{
	//is_startScan=0;
	int *p=(int *)id;

	if (id < 0 && id >= MAX_CHANNEL)
	{
		return -1;
	}
	if (pCDecoder[id] == NULL) return -1;
	pCDecoder[id]->is_start=0;
	pCDecoder[id]->DecodeVDataFun = NULL;
	pCDecoder[id]->DecodeVDataFunEx = NULL;
	if(g_DecoderNum)g_DecoderNum--;
	delete pCDecoder[id];
	pCDecoder[id]=NULL;

	return 0;
}


//**************相关函数******************
//任务创建
static int OS_tskCreate(OS_TskHndl *pPrc, OS_ThrEntryFunc func,Uint32 initState,void *appData,Uint32 pri,Uint32 stackSize)
{
  int status=OS_SOK;
  pthread_attr_t thread_attr;
  struct sched_param schedprm;

  status = pthread_attr_init(&thread_attr);
  if(status!=OS_SOK) {
    printf("OSA_thrCreate() - Could not initialize thread attributes\n");
    return status;
  }
  pPrc->TskCount=0;
  
  pPrc->curState      = initState;
  pPrc->pri=pri;
  pPrc->stackSize=stackSize;

    if(stackSize!=OS_THR_STACK_SIZE_DEFAULT)
    pthread_attr_setstacksize(&thread_attr, stackSize);

  status |=pthread_attr_setdetachstate(&thread_attr,PTHREAD_CREATE_DETACHED);
  status |= pthread_attr_setinheritsched(&thread_attr, PTHREAD_EXPLICIT_SCHED);
  status |= pthread_attr_setschedpolicy(&thread_attr, SCHED_FIFO);
    
  if((int)pri>OS_THR_PRI_MAX)   
    pri=(int)OS_THR_PRI_MAX;

  if((int)pri<OS_THR_PRI_MIN)   
    pri=(int)OS_THR_PRI_MIN;
    
  schedprm.sched_priority = pri;
  status |= pthread_attr_setschedparam(&thread_attr, &schedprm);

  if(status!=OS_SOK) {
    printf("OS_thrCreate() - Could not initialize thread attributes\n");
    goto error_exit;
  }

  status = pthread_create(&pPrc->thrHndl, &thread_attr, func, (void*)appData);
  
  if(status!=OS_SOK) {
	  printf("OS_thrCreate() - Could not create thread [%d],name:[%s]\n", status,pPrc->TskName);
	}
  
error_exit:  
	pthread_attr_destroy(&thread_attr);

  return status;
}

//***************任务锁***************
/*
static int OS_tskMutexCreate(OS_MutexHndl *hndl)
{
  pthread_mutexattr_t mutex_attr;
  int status=OS_SOK;
 
  status |= pthread_mutexattr_init(&mutex_attr);
  status |= pthread_mutex_init(&hndl->lock, &mutex_attr);
  
  if(status!=OS_SOK)
    printf("OS_mutexCreate() = %d \r\n", status);

  pthread_mutexattr_destroy(&mutex_attr);
    
  return status;
}

static int OS_tskMutexDelete(OS_MutexHndl *hndl)
{
	pthread_mutex_destroy(&hndl->lock);  
	return OS_SOK;
}

static void OS_tskMutexLock(OS_MutexHndl *hndl)
{
	pthread_mutex_lock(&hndl->lock);
}

static void OS_tskMutexUnlock(OS_MutexHndl *hndl)
{
	pthread_mutex_unlock(&hndl->lock);
}
*/

